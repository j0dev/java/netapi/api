package nl.j0dev.netapi.api.transformer;

import nl.j0dev.netapi.api.msg.MessageHandler;

/**
 * Message transformer with default channel name
 */
public interface MessageTopicTransformer extends MessageHandler {
    /**
     * Get the default channel name to send corresponding messages
     * @return default object channel
     */
    String getChannelName();
}
