package nl.j0dev.netapi.api.transformer;

import java.util.HashMap;
import java.util.Map;

/**
 * Repository for classes that transform messages into objets and vice versa
 */
public class TransformerRepositoryImpl implements TransformerRepository {
    /**
     * Transformers map
     */
    Map<String, MessageTransformer<?>> transformers = new HashMap<>();

    /**
     * Add a class that can take care of translating an object
     * @param klass Fully Qualified Class Name
     * @param transformer Transformer class
     */
    public void addTransformer(String klass, MessageTransformer<?> transformer) {
        transformers.put(klass, transformer);
    }

    /**
     * Retrieve a transformer for a class
     * @param klass Fully Qualified Class Name
     * @return Transformer class
     */
    public MessageTransformer<?> getMessageTransformer(String klass) {
        return this.transformers.get(klass);
    }
}
