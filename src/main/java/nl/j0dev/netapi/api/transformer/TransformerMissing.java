package nl.j0dev.netapi.api.transformer;

public class TransformerMissing extends Exception {
    final String klass;

    public TransformerMissing(String klass) {
        super("Transformer class not found for class '"+ klass +"'");
        this.klass = klass;
    }

    public String getKlass() {
        return this.klass;
    }
}
