package nl.j0dev.netapi.api.transformer;

/**
 * Repository for classes that transform messages into objets and vice versa
 */
public interface TransformerRepository {
    /**
     * Add a class that can take care of translating an object
     * @param klass Fully Qualified Class Name
     * @param transformer Transformer class
     */
    public void addTransformer(String klass, MessageTransformer<?> transformer);

    /**
     * Retrieve a transformer for a class
     * @param klass Fully Qualified Class Name
     * @return Transformer class
     */
    public MessageTransformer<?> getMessageTransformer(String klass);
}
