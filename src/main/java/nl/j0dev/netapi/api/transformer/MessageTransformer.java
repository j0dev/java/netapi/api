package nl.j0dev.netapi.api.transformer;

/**
 * Message transformer allows serialization and deserialization for the netapi system
 * @param <T> Object to transform
 */
public interface MessageTransformer<T> {
    /**
     * Transforms the class into a byte representation to send
     * @param data Input class
     * @return Byte representation
     */
    T deserialize(byte[] data);

    /**
     * Transforms a byte representation back into the class
     * @param data Byte representation
     * @return Class
     */
    byte[] serialize(T data);
}
