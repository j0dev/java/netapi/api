package nl.j0dev.netapi.api;

import nl.j0dev.netapi.api.msg.MessageSubsystem;
import nl.j0dev.netapi.api.sd.ServiceDiscovery;
import nl.j0dev.netapi.api.transformer.TransformerRepository;

/**
 * Network Application
 */
public interface NetApp {
    /**
     * Service Discovery subsystem
     */
    ServiceDiscovery sd();

    /**
     * Messaging subsystem
     */
    MessageSubsystem msg();

    /**
     * Class transformer subsystem
     */
    TransformerRepository transformer();
}
