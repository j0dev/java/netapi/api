package nl.j0dev.netapi.api.sd;

import java.util.List;

/**
 * Basic information about another NetApp
 */
public interface NetNode {
    /**
     * Get the name of the NetApp
     */
    String getName();

    /**
     * Get all the capabilities of the NetApp
     */
    List<String> getCapabilities();
    /**
     * Check if the NetApp has a specific capability
     * @param capability capability to check for
     */
    boolean hasCapability(String capability);

    /**
     * Get the network identifier to communicate with it
     */
    String getNetID();
}
