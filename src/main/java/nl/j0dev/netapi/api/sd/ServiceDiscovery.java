package nl.j0dev.netapi.api.sd;

import java.util.List;

/**
 * NetApp Service Discovery subsystem
 */
public interface ServiceDiscovery {
    /**
     * Service Discovery management for the current app
     */
    NetNodeSelf self();

    /**
     * Get all known nodes
     */
    List<NetNode> getNodes();

    /**
     * Find nodes by capability
     * @param capability capability to look for
     */
    List<NetNode> findNodes(String capability);
}
