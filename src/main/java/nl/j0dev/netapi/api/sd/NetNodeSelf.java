package nl.j0dev.netapi.api.sd;

/**
 * Service Discovery management for current app
 */
public interface NetNodeSelf extends NetNode {
    /**
     * Add another capability
     * @param capability to add
     */
    void addCapability(String capability);

    /**
     * Remove a capability
     * @param capability to remove
     */
    void removeCapability(String capability);

    /**
     * Change the name of the app
     * @param newName new name
     */
    void changeName(String newName);
}
