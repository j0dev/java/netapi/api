package nl.j0dev.netapi.api.exceptions;

public class ChannelMissingException extends Exception {
    final String klass;

    public ChannelMissingException(String klass) {
        super("Channel annotation missing for class: "+ klass);
        this.klass = klass;
    }

    public String getKlass() {
        return this.klass;
    }
}
