package nl.j0dev.netapi.api.msg.handlers;

/**
 * Handler for handling service requests
 */
public interface RpcObjectHandler<T> {
    /**
     * Handles an incoming message
     * @implNote gets triggered for each message received on the subscribed channel, and message data is transformed into the corresponding class before being called
     * @param channel Channel on which the message was received
     * @param message Data object that was sent in the message
     * @param replyChannel (optional) Reply channel
     * @return Message to return
     */
    Object onMessage(String channel, T message, String replyChannel);
}
