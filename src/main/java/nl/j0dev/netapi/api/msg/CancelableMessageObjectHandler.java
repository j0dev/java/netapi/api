package nl.j0dev.netapi.api.msg;

import nl.j0dev.netapi.api.NetApp;

/**
 * Cancelable handler class for handling received messages
 */
public abstract class CancelableMessageObjectHandler<T> implements MessageObjectHandler<T> {
    /**
     * NetApp reference
     * @implNote Only available after registration
     */
    protected NetApp netApp;

    /**
     * Binds the NetApp during registration to allow canceling
     * @implNote Used by the registration system
     * @param netApp NetApp reference
     */
    public void bindCancel(NetApp netApp) {
        this.netApp = netApp;
    }

    /**
     * Cancel the subscription
     * @apiNote Available only after registration
     */
    public void cancel() {
        if (this.netApp != null) {
            this.netApp.msg().unsubscribe(this);
        }
    }
}
