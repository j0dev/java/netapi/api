package nl.j0dev.netapi.api.msg;

/**
 * Handler class for handling received messages
 */
@FunctionalInterface
public interface MessageHandler {
    /**
     * Handles an incoming message
     * @implNote gets triggered for each message received on the subscribed channel
     * @param channel Channel on which the message was received
     * @param message Data that was sent in the message
     * @param replyChannel (optional) Reply channel
     */
    void onMessage(String channel, byte[] message, String replyChannel);
}
