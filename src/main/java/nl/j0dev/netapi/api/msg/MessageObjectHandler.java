package nl.j0dev.netapi.api.msg;

/**
 * Handler class for handling received messages
 */
@FunctionalInterface
public interface MessageObjectHandler<T> {
    /**
     * Handles an incoming message
     * @implNote gets triggered for each message received on the subscribed channel, and message data is transformed into the corresponding class before being called
     * @param channel Channel on which the message was received
     * @param message Data object that was sent in the message
     * @param replyChannel (optional) Reply channel
     */
    void onMessage(String channel, T message, String replyChannel);
}
