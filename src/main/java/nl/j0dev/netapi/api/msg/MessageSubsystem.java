package nl.j0dev.netapi.api.msg;

import nl.j0dev.netapi.api.exceptions.ChannelMissingException;
import nl.j0dev.netapi.api.msg.handlers.RpcHandler;
import nl.j0dev.netapi.api.msg.handlers.RpcObjectHandler;
import nl.j0dev.netapi.api.transformer.TransformerMissing;

/**
 * Message handling subsystem
 *
 * @apiNote Cancelable handlers still require a little more work before standardization
 */
public interface MessageSubsystem {
    /**
     * Publish / Send data to a channel
     * @param channel Channel to send on
     * @param data Data / Message to send
     */
    void publish(String channel, byte[] data);
    /**
     * Publish / Send data to a channel
     * @param channel Channel to send on
     * @param data Data / Message to send
     * @param replyChannel Reply channel
     */
    void publish(String channel, byte[] data, String replyChannel);
    /**
     * Publish / Send an object to a channel
     * @param channel Channel to send on
     * @param object Data / Message object to encode and send
     */
    void publish(String channel, Object object) throws TransformerMissing;
    /**
     * Publish / Send an object to a channel
     * @param channel Channel to send on
     * @param object Data / Message object to encode and send
     * @param replyChannel Reply channel
     */
    void publish(String channel, Object object, String replyChannel) throws TransformerMissing;
    /**
     * Publish / Send an object to a channel
     * @param object Data / Message object to encode and send
     */
    void publish(Object object) throws TransformerMissing;
    /**
     * Publish / Send an object to a channel
     * @param object Data / Message object to encode and send
     * @param replyChannel Reply channel
     */
    void publish(Object object, String replyChannel) throws TransformerMissing;

    /**
     * Subscribe / listen for messages
     * Note: Needs a Channel annotation or channel argument
     * @param messageHandler Message handler
     */
    void subscribe(MessageHandler messageHandler) throws ChannelMissingException;
    /**
     * Subscribe / listen for messages
     * Note: Needs a Channel annotation or channel argument
     * @param channel Channel to listen on
     * @param messageHandler Message handler
     */
    void subscribe(String channel, MessageHandler messageHandler);

    /**
     * Subscribe / listen for objects
     * Note: Needs a Channel annotation or channel argument
     * @param messageObjectHandler Message object handler
     */
    void subscribe(MessageObjectHandler<?> messageObjectHandler) throws ChannelMissingException;
    /**
     * Subscribe / listen for objects
     * Note: Needs a Channel annotation or channel argument
     * @param channel Channel to listen on
     * @param messageObjectHandler Message object handler
     */
    void subscribe(String channel, MessageObjectHandler<?> messageObjectHandler);

    /**
     * Subscribe an RPC Handler
     * Note: Needs a Channel annotation or channel argument
     * @param handler RPC Handler to register
     */
    void subscribe(RpcHandler handler) throws ChannelMissingException;
    /**
     * Subscribe an RPC Handler
     * Note: Needs a Channel annotation or channel argument
     * @param channel Channel to listen on
     * @param handler RPC Handler to register
     */
    void subscribe(String channel, RpcHandler handler);

    /**
     * Subscribe an RPC Handler
     * Note: Needs a Channel annotation or channel argument
     * @param handler RPC Handler to register
     */
    void subscribe(RpcObjectHandler<?> handler) throws ChannelMissingException;
    /**
     * Subscribe an RPC Handler
     * Note: Needs a Channel annotation or channel argument
     * @param channel Channel to listen on
     * @param handler RPC Handler to register
     */
    void subscribe(String channel, RpcObjectHandler<?> handler);

    /**
     * Send a request on a channel and listen\ for a response
     * @param channel Channel to request to
     * @param data Data / message to request with
     * @param messageHandler Cancelable message handler to handle (one or more) responses
     */
    void request(String channel, byte[] data, CancelableMessageHandler messageHandler);
    /**
     * Send a request on a channel and listen for a response
     * @param channel Channel to request to
     * @param data Data / message to request with
     * @param messageObjectHandler Cancelable message object handler to handle (one or more) responses objects
     */
    void request(String channel, Object data, CancelableMessageObjectHandler<?> messageObjectHandler) throws TransformerMissing;

    /**
     * Unsubscribe a message handler
     * @param handler Message handler to unsubscribe
     */
    void unsubscribe(Object handler);
}
