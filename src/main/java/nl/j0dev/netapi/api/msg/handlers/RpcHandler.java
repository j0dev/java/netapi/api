package nl.j0dev.netapi.api.msg.handlers;

/**
 * Handler for handling service requests
 */
public interface RpcHandler {
    /**
     * Handles an incoming message
     * @implNote gets triggered for each message received on the subscribed channel
     * @param channel Channel on which the message was received
     * @param message Data that was sent in the message
     * @param replyChannel (optional) Reply channel
     * @return Message to return
     */
    byte[] onMessage(String channel, byte[] message, String replyChannel);
}
