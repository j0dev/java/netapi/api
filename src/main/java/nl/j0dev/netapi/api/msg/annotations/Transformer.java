package nl.j0dev.netapi.api.msg.annotations;

import nl.j0dev.netapi.api.transformer.MessageTransformer;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Transformer class used (de)serialize the object
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Transformer {
    Class<MessageTransformer<?>> transformer();
}
